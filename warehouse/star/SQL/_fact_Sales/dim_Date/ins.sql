﻿declare @date datetime;

set @date = N'1999/3/13'

insert 
	Dim_Date
	(
		date,
		day
		,
		day_of_week
		,
		month
		,
		month_name
		,
		quarter
		,
		quarter_name
		,
		year
	)
	output inserted.*
	values(
		@date,
		DAY(@date)
		,
		DATEPART(WEEKDAY, @date)
		,
		DATEPART(month, @date)
		,
		datepart(month,@date)
		,
		datepart(quarter, @date)
		,
		datepart(quarter, @date)
		,
		datepart(year,@date)
	)
