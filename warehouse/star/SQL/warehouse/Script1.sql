﻿SELECT
	P.Brand,
	S.store_country AS Countries,
	SUM(F.quantity) total

FROM Fact_Sales F
INNER JOIN Dim_Date D    ON (F.time = D.Id)
INNER JOIN Dim_Store S   ON (F.Store = S.Id)
INNER JOIN Dim_Product P ON (F.Product = P.Id)

WHERE D.Year = 1997 AND  P.Product_Category = 'web'

GROUP BY
	P.Brand,
	S.store_country
