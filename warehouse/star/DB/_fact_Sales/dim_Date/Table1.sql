﻿CREATE TABLE [dbo].[Dim_Date]
(
	[Id] bigINT NOT NULL PRIMARY KEY identity
	,
	date datetime
	,
	day int, 
    [day_of_week] INT NULL, 
    [month] INT NULL, 
    [month_name] NVARCHAR(50) NULL, 
    [quarter] INT NULL, 
    [quarter_name] NVARCHAR(50) NULL, 
    [year] INT NULL

)
