﻿CREATE TABLE [dbo].[Dim_Store]
(
	[Id] BIGINT NOT NULL PRIMARY KEY identity, 
    [store_number] INT NULL, 
    [store_province] NVARCHAR(50) NULL, 
    [store_country] NVARCHAR(50) NULL
)
