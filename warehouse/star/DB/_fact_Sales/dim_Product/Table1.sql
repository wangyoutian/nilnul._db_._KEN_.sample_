﻿CREATE TABLE [dbo].[Dim_Product]
(
	[Id] BIGINT NOT NULL PRIMARY KEY identity, 
    [ean_code] NVARCHAR(50) NULL, 
    [product_name] NVARCHAR(50) NULL, 
    [brand] NVARCHAR(50) NULL, 
    [product_category] NVARCHAR(50) NULL
)
