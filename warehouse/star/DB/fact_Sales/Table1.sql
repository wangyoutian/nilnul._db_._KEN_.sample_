﻿CREATE TABLE [dbo].Fact_Sales
(
	[id] bigINT NOT NULL PRIMARY KEY identity

	,
	time bigint references [Dim_Date](id)
	,
	store bigint references [Dim_Store](id)
	,
	product bigint references [Dim_Product](id)
	,
	quantity bigint	not null	--quantity sold
)
